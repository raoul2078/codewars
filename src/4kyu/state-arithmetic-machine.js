//https://www.codewars.com/kata/54c1bf903f0696f04600068b/javascript

const cpu = {
  readReg: function (name) {},
  writeReg: function (name, value) {},
  popStack: function () {},
  writeStack: function (value) {},
};
function Machine(cpu) {
  function parseInstruction(instruction) {
    let [operationData, reg2] = instruction.split(',');
    let [operation, arg] = operationData.split(' ');

    if (operation !== undefined) {
      operation = operation.trim();
    }
    if (arg !== undefined) {
      arg = arg.trim();
    }
    if (reg2 !== undefined) {
      reg2 = reg2.trim();
    }

    return { operation, arg, reg2 };
  }

  // let { operation, arg, secondArg } = parseInstruction("push 23");
  // parseInstruction("add 5,b");
  // parseInstruction("push 23");

  const registers = ['a', 'b', 'c', 'd'];
  const stackOperations = [
    'push',
    'pop',
    'pushr',
    'pushrr',
    'pushrr',
    'popr',
    'poprr',
  ];
  const arithmeticOperations = [
    'add',
    'adda',
    'sub',
    'suba',
    'mul',
    'mula',
    'div',
    'diva',
    'and',
    'anda',
    'or',
    'ora',
    'xor',
    'xora',
  ];

  function getValue(arg) {
    return isRegister(arg) ? parseInt(cpu.readReg(arg)) : parseInt(arg);
  }
  function isRegister(str) {
    return registers.includes(str);
  }

  function add(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) + parseInt(current)
    );
  }

  function sub(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) - parseInt(current)
    );
  }

  function mul(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) * parseInt(current)
    );
  }

  function div(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) / parseInt(current)
    );
  }

  function and(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) & parseInt(current)
    );
  }

  function or(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) | parseInt(current)
    );
  }

  function xor(arr) {
    return arr.reduce(
      (previous, current) => parseInt(previous) ^ parseInt(current)
    );
  }

  function handleStackOperation(instruction) {
    const { operation, arg } = parseInstruction(instruction);
    const value = getValue(arg);
    switch (operation) {
      // stack operations
      case 'push':
        cpu.writeStack(value);
        break;
      case 'pop':
        cpu.writeReg(arg, cpu.popStack());
        break;
      case 'pushr':
        cpu.writeStack(cpu.readReg('a'));
        cpu.writeStack(cpu.readReg('b'));
        cpu.writeStack(cpu.readReg('c'));
        cpu.writeStack(cpu.readReg('d'));
        break;
      case 'pushrr':
        cpu.writeStack(cpu.readReg('d'));
        cpu.writeStack(cpu.readReg('c'));
        cpu.writeStack(cpu.readReg('b'));
        cpu.writeStack(cpu.readReg('a'));
        break;
      case 'popr':
        cpu.writeReg('d', cpu.popStack());
        cpu.writeReg('c', cpu.popStack());
        cpu.writeReg('b', cpu.popStack());
        cpu.writeReg('a', cpu.popStack());
        break;
      case 'poprr':
        cpu.writeReg('a', cpu.popStack());
        cpu.writeReg('b', cpu.popStack());
        cpu.writeReg('c', cpu.popStack());
        cpu.writeReg('d', cpu.popStack());
        break;
      default:
        break;
    }
  }

  function handleArithmeticOperation(instruction) {
    let { operation, arg, reg2 } = parseInstruction(instruction);
    reg2 = reg2 ? reg2 : 'a';
    const value = getValue(arg);
    const lastLetter = operation.charAt(operation.length - 1);
    if (lastLetter === 'a') {
      operation = operation.slice(0, operation.length - 1);
      cpu.writeStack(cpu.readReg(lastLetter));
    }
    const values = [];
    for (let i = 0; i < value; i++) {
      values.push(cpu.popStack());
    }
    //console.log(values);
    switch (operation) {
      case 'add':
        cpu.writeReg(reg2, add(values));
        break;
      case 'sub':
        console.log(reg2, values);
        cpu.writeReg(reg2, sub(values));
        break;
      case 'mul':
        cpu.writeReg(reg2, mul(values));
        break;
      case 'div':
        cpu.writeReg(reg2, div(values));
        break;
      case 'and':
        cpu.writeReg(reg2, and(values));
        break;
      case 'or':
        cpu.writeReg(reg2, or(values));
        break;
      case 'xor':
        cpu.writeReg(reg2, xor(values));
        break;
      default:
        break;
    }
  }

  /*
  int readReg(String name): Returns the value of the named register.
  void writeReg(String name, int value): Stores the value into the given register.
  int popStack(): Pops the top element of the stack, returning the value.
  void writeStack(int value): Pushes an element onto the stack.
  */
  function exec(instruction) {
    const { operation, arg, reg2 } = parseInstruction(instruction);
    const value = getValue(arg);

    if (operation === 'mov' && reg2 !== undefined) {
      cpu.writeReg(reg2, value);
    } else if (stackOperations.includes(operation)) {
      handleStackOperation(instruction);
    } else if (arithmeticOperations.includes(operation)) {
      handleArithmeticOperation(instruction);
    }
  }

  return { exec };
}
