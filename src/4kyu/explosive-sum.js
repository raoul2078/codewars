function sum(num) {
  if (num < 0) {
    return 0;
  }
  if (num === 0) {
    return 1;
  }

  let k1 = GeneralizedPentagonalNum(num);
  let sign = Math.pow(-1, num);
  let k2 = GeneralizedPentagonalNum(num + 1);
  return sum(num - k1) + sign * sum(num - k2);
}

function GeneralizedPentagonalNum(n) {
  /* 
    GP(n) = {
      m(3m+1)/2 , n = 2m
      m(3m-1)/2 , n=2m-1
    }
   */
  if (n % 2 === 0) {
    let m = n / 2;
    return (m * (3 * m + 1)) / 2;
  } else {
    let m = (n + 1) / 2;
    return (m * (3 * m - 1)) / 2;
  }
}
