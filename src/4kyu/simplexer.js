// https://www.codewars.com/kata/54b8204dcd7f514bf2000348/train
function Token(value, type) {
  this.value = value;
  this.type = type;
}

/*
 * integer : Any sequence of one or more digits.

boolean : true or false.

string : Any sequence of characters surrounded by "double quotes".

operator : The characters +, -, *, /, %, (, ), and =.

keyword : The following are keywords: if, else, for, while, return, func, and break.

whitespace : Matches standard whitespace characters (space, newline, tab, etc.)
Consecutive whitespace characters should be matched together.

identifier : Any sequence of alphanumber characters, as well as underscore and dollar sign,
and which doesn't start with a digit. Make sure that keywords aren't matched as identifiers!

 */
class Simplexer {
  constructor(buffer) {
    this.input = buffer;

    this.regexs = {
      integer: /^(\d+)/,
      boolean: /^(true|false)/,
      string: /^(".+")/,
      operator: /^(\+|-|\*|\/|=|%|\(|\))/,
      keyword: /^(if|else|for|while|return|func|break)/,
      whitespace: /^(\s+)/,
      identifier: /^([a-zA-Z0-9_]|\$)+/,
    };

    this.hasNext = function () {
      return this.input !== undefined && this.input.length > 0;
    };
    this.next = function () {
      for (const [type, regex] of Object.entries(this.regexs)) {
        if (regex.test(this.input)) {
          const [, value] = this.input.match(regex);
          this.input = this.input.slice(`${value}`.length);
          return new Token(value, type);
        }
      }
      this.input = '';
      return new Token('', '');
    };
  }
}
