function hasDuplicates(arr) {
  for (let i = 0; i < arr.length; i++) {
    let item = arr[i];
    if (arr.indexOf(item, i + 1) > -1) {
      return true;
    }
  }
  return false;
}
function validRows(board) {
  for (let i = 0; i < 9; i++) {
    if (hasDuplicates(board[i])) {
      return false;
    }
  }
  return true;
}

function validColumns(board) {
  for (let i = 0; i < 9; i++) {
    let col = [];
    for (let j = 0; j < 9; j++) {
      col.push(board[j][i]);
    }
    if (hasDuplicates(col)) {
      return false;
    }
  }
  return true;
}

function validRegions(board) {
  let regions = [[], [], [], [], [], [], [], [], []];
  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      if (i < 3) {
        if (j < 3) {
          regions[0].push(board[i][j]);
        } else if (j < 6) {
          regions[1].push(board[i][j]);
        } else if (j < 9) {
          regions[2].push(board[i][j]);
        }
      } else if (i < 6) {
        if (j < 3) {
          regions[3].push(board[i][j]);
        } else if (j < 6) {
          regions[4].push(board[i][j]);
        } else if (j < 9) {
          regions[5].push(board[i][j]);
        }
      } else if (i < 9) {
        if (j < 3) {
          regions[6].push(board[i][j]);
        } else if (j < 6) {
          regions[7].push(board[i][j]);
        } else if (j < 9) {
          regions[8].push(board[i][j]);
        }
      }
    }
  }
  for (let region of regions) {
    if (hasDuplicates(region)) {
      return false;
    }
  }
  return true;
}

function doneOrNot(board) {
  //your code here
  let checkRows = validRows(board);
  let checkColumns = validColumns(board);
  let checkRegions = validRegions(board);

  if (checkRows && checkColumns && checkRegions) {
    return 'Finished!';
  }
  return 'Try again!';
}
