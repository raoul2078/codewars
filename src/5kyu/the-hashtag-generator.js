function generateHashtag(str) {
  let input = str.trim();
  if (input.length < 1) {
    return false;
  }
  let inputArray = str.split(' ');
  inputArray.forEach((item, index) => {
    inputArray[index] = item.charAt(0).toUpperCase() + item.slice(1);
  });
  let finalString = '#' + inputArray.join('');
  return finalString.length > 140 ? false : finalString;
}
