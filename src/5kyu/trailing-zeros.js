function zeros(n) {
  let count = 0;
  let factors = [];
  for (let i = 2; i <= n; i++) {
    factors.push(i);
  }
  let size = factors.length;
  let ten = [];
  for (let i = 0; i < size; i++) {
    ten.push(tenFactors(getDivisors(factors[i])));
  }
  ten = ten.flat(2);
  //console.log(ten);
  while (ten.includes(2) && ten.includes(5)) {
    ten = tenFactors(ten);
    //console.log(ten);
  }
  count = ten.filter((item) => item === 10).length;
  console.log(count, ten);
  return count;
}

function getDivisors(n) {
  let factor = 2;
  let factors = [];
  while (factor * factor <= n) {
    if (n % factor === 0) {
      while (n % factor === 0) {
        factors.push(factor);
        n /= factor;
      }
    }
    factor = factor === 2 ? 3 : factor + 2;
  }
  if (n > 1) {
    factors.push(n);
  }
  return factors;
}

function tenFactors(divisors) {
  let two = divisors.filter((item) => item === 2);
  let five = divisors.filter((item) => item === 5);
  let others = divisors.filter((item) => item !== 2 && item !== 5);
  let numOfTens = two.length < five.length ? two.length : five.length;
  let ten = [];
  for (let i = 1; i <= numOfTens; i++) {
    ten.push(10);
  }
  two.splice(0, numOfTens);
  five.splice(0, numOfTens);
  others.push(two);
  others.push(five);
  return [ten, others].flat(2);
}

zeros(100);
