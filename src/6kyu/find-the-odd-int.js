function findOdd(A) {
  let result = {};
  for (let i of A) {
    result[i] = result[i] ? result[i] + 1 : 1;
  }
  for (let i in result) {
    if (result[i] % 2 != 0) {
      return parseInt(i);
    }
  }
  return 0;
}
